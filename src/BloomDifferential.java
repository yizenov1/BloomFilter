import java.util.Random;
import java.io.*;

public class BloomDifferential {
	
	BloomFilterDet bloomFilter;
	
	int number_of_hash_functions;
	int bitsPerElement;
	int setSize;
	byte[] salts;
	
	String diff_file_path;
	String grams_file_path;
	//String database_file_path;
	
	BloomDifferential(int setSize, int bitsPerElement, int number_of_hash_functions) {
		
		this.setSize = setSize;
		this.bitsPerElement = bitsPerElement;
		this.number_of_hash_functions = number_of_hash_functions;
		
		this.salts = new byte[number_of_hash_functions];
		new Random().nextBytes(salts);
		
		diff_file_path = "src/Files/DiffFile.txt";
		//database_file_path = "src/Files/database.txt";
		grams_file_path = "src/Files/grams.txt";
	}
	
	BloomFilterDet createFilter() {
		
		bloomFilter = new BloomFilterDet(setSize, bitsPerElement, number_of_hash_functions, salts);
		ReadDiffFile(diff_file_path);
		
		return bloomFilter;
	}
	
	String RetrieveRecord(String s) {
		if (bloomFilter.Appears(s)) {
			return s + " (FROM THE FILTER)";
		} else {
			return RetrieveRecordFromDatabase(s, grams_file_path);
		}
	}
	
	String RetrieveRecordFromDatabase(String s, String fileName) {

        String line = null;
        boolean isFound = false;

        try {
        	
            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while((line = bufferedReader.readLine()) != null) {
            	if (line.equals(s)) {
            		isFound = true;
            		break;
            	}
            }
            
            bufferedReader.close();
            fileReader.close();
        }
        catch(FileNotFoundException ex) {
            System.out.println("Unable to open file '" + fileName + "'");
        }
        catch(IOException ex) {
            System.out.println("Error reading file '" + fileName + "'");
        }
        
        if (isFound) {
        	return s;
        }
        return "";
	}
	
	void ReadDiffFile(String fileName) {

        String line = null;
        
        String whole_line;
        String[] words;

        try {
        	
            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while((line = bufferedReader.readLine()) != null) {
            	
            	whole_line = "";
            	words = line.split(" ");
            	
            	for (int i = 0; i < 4; i++) {
            		if (i > 0) { whole_line += " "; }
            		whole_line += words[i];
            	}
            	
            	bloomFilter.Add(whole_line);
            }   

            bufferedReader.close();
            fileReader.close();
        }
        catch(FileNotFoundException ex) {
            System.out.println("Unable to open file '" + fileName + "'");
        }
        catch(IOException ex) {
            System.out.println("Error reading file '" + fileName + "'");
        }
	}

}

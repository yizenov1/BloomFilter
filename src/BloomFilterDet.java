import java.util.BitSet;
import java.math.BigInteger;

public class BloomFilterDet {

	BitSet bloomFilter;
	
	final BigInteger FNV64PRIME = new BigInteger("109951168211");
	final BigInteger FNV_64INIT = new BigInteger("14695981039346656037");
	
	byte[] salts;
	byte[] bytes;
	BigInteger h;
	int set_index;
	
	int number_of_hash_functions;
	int bitsPerElement;
	int setSize;
	int number_of_added_elements;
	
	BloomFilterDet(int setSize, int bitsPerElement, int number_of_hash_functions, byte[] salts) {
		
		bloomFilter = new BitSet(setSize * bitsPerElement);
		
		this.salts = salts;
		
		this.setSize = setSize;
		this.bitsPerElement = bitsPerElement;
		this.number_of_hash_functions = number_of_hash_functions;
		number_of_added_elements = 0;
	}
	
	void Add(String s) {
		
		s = s.toLowerCase();
		bytes = s.getBytes();
		
		for (int i = 0; i < number_of_hash_functions; i++) {
			set_index = Math.abs(FNV_64(bytes, salts[i]).intValue() % bloomFilter.size());		
			bloomFilter.set(set_index);
		}
		
		number_of_added_elements++;
	}
	
	BigInteger FNV_64(byte[] bytes, byte salt) {
		
		h = FNV_64INIT;
		
		for (int i = 0; i < bytes.length; i++) {
			h = h.xor(BigInteger.valueOf(bytes[i]));
			h = (h.multiply(FNV64PRIME)).mod(BigInteger.valueOf((long) (Math.pow(2, 64))));
		}
		
		h = h.xor(BigInteger.valueOf(salt));
		h = (h.multiply(FNV64PRIME)).mod(BigInteger.valueOf((long) (Math.pow(2, 64))));
		
		return h;
	}
	
	boolean Appears(String s) {
		
		s = s.toLowerCase();
		bytes = s.getBytes();
		
		for (int i = 0; i < number_of_hash_functions; i++) {	
			set_index = Math.abs(FNV_64(bytes, salts[i]).intValue() % bloomFilter.size());		
			if (bloomFilter.get(set_index) == false) {
				return false;
			}
		}
		
		return true;
	}
	
	int filterSize() {
		return setSize * bitsPerElement;
	}
	
	int dataSize() {	
		return number_of_added_elements;
	}
	
	int numHashes() {
		return number_of_hash_functions;
	}
}

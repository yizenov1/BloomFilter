public class LaunchBloomFilter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int[] bitsPerElement = {4, 8, 10};
		FalsePositives evaluation;
		
		int setSize = 1000; // elements for the filter
		int extraSetSize = 500; // elements that are not in the filter
		int querySize = setSize + extraSetSize;

		for (int i = 0; i < bitsPerElement.length; i++) {
			evaluation = new FalsePositives(setSize, extraSetSize, querySize, bitsPerElement[i]);
			evaluation.RunEvaluation();
		}
		
		//EmpiricalComparison empericalComparison = new EmpiricalComparison(1000);
		//empericalComparison.Run();

	}

}

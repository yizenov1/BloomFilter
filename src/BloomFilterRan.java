import java.util.BitSet;
import java.util.Random;

public class BloomFilterRan {
	
	BitSet bloomFilter;
	
	int a, b;
	int prime_number;
	
	byte[] salts;
	byte[] bytes;
	int set_index;
	long hashcode;
	int byte_array_length;
	
	int number_of_hash_functions;
	int bitsPerElement;
	int setSize;
	int number_of_added_elements;

	BloomFilterRan(int setSize, int bitsPerElement, int number_of_hash_functions, byte[] salts) {
		
		bloomFilter = new BitSet(setSize * bitsPerElement);
		
		Random generator = new Random();
		a = generator.nextInt(setSize);
		b = generator.nextInt(setSize);

		this.salts = salts;
		
		prime_number = findPrimeNumber(setSize);
		
		this.setSize = setSize;
		this.bitsPerElement = bitsPerElement;
		this.number_of_hash_functions = number_of_hash_functions;
		number_of_added_elements = 0;
	}
	
	int findPrimeNumber(int setSize) {
		
		int primeNumber = setSize;
		int counter = 0;
		
		while (primeNumber < setSize * bitsPerElement) {
		
	        for(int num = primeNumber; num >= 1; num--) {
	        	if(primeNumber % num == 0) {
	        		counter = counter + 1;
	        		if (counter > 2) {
	        			break;
	        		}
			    }
			}
	        
			if (counter == 2) {
				return primeNumber;
			}	
		     
			primeNumber++;
		}
		
		return primeNumber;
	}

	void Add(String s) {
		
		s = s.toLowerCase();
		bytes = s.getBytes();
		
		for (int i = 0; i < number_of_hash_functions; i++) {
			set_index = Math.abs(randomHashing(bytes, salts[i]) % bloomFilter.size());
			bloomFilter.set(set_index);
		}
		
		number_of_added_elements++;
	}
	
	int randomHashing(byte[] bytes, byte salt) {
		
		byte_array_length = bytes.length;
		hashcode = 0;
		
		for (int i = 0; i < bytes.length; i++) {
			hashcode += bytes[i] * (byte) Math.pow(63, (byte_array_length - i));
		}
		hashcode += salt * (byte) 1;

		return (int) (a * hashcode + b) % prime_number;
	}
	
	boolean Appears(String s) {
		
		s = s.toLowerCase();
		bytes = s.getBytes();
		
		for (int i = 0; i < number_of_hash_functions; i++) {
			set_index = Math.abs(randomHashing(bytes, salts[i]) % bloomFilter.size());
			if (bloomFilter.get(set_index) == false) {
				return false;
			}
		}
		
		return true;
	}
	
	int filterSize() {
		return setSize * bitsPerElement;
	}
	
	int dataSize() {	
		return number_of_added_elements;
	}
	
	int numHashes() {
		return number_of_hash_functions;
	}
}

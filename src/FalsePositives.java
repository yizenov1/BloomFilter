import java.util.Random;

public class FalsePositives {
	
	BloomFilterDet bloomFilterDet;
	BloomFilterRan bloomFilterRan;
	
	StringBuilder rand_string;
	Random generator;
	
	String[] randomStringSet;
	String[] extra_randomStringSet;
	byte[] salts;
	String[] queryWordSet;
	
	final int bitsPerElement;
	int setSize;
	int extraSetSize;
	int querySize;
	int number_of_hash_functions;
	double numb = Math.log(2) / Math.log(Math.E);
	
	FalsePositives(int setSize, int extraSetSize, int querySize, int bitsPerElement) {
		
		generator = new Random(); 
	    rand_string = new StringBuilder();
	    
	    randomStringSet = new String[setSize];
	    queryWordSet = new String[querySize];
	    extra_randomStringSet = new String[extraSetSize];
	    
	    this.setSize = setSize;
		this.bitsPerElement = bitsPerElement;
	    this.extraSetSize = extraSetSize;
	    this.querySize = querySize;
	    
	    number_of_hash_functions = (int) (numb * (double) bitsPerElement);
	    
	    salts = new byte[number_of_hash_functions];
	    generator.nextBytes(salts);
	   
	    bloomFilterDet = new BloomFilterDet(setSize, bitsPerElement, number_of_hash_functions, salts);
	    bloomFilterRan = new BloomFilterRan(setSize, bitsPerElement, number_of_hash_functions, salts);
	    
	    FillOutFilters();
	    FillOutExtraSet();
	    FillOutQuerySet();
	}
	
	void FillOutFilters() {
		
		String word;
	    
	    for (int i = 0; i < setSize; i++) {
	    	
	    	word = randomString();
	    	randomStringSet[i] = word;
	    	
	    	//System.out.println(word);
	    	
	    	bloomFilterDet.Add(word);
			bloomFilterRan.Add(word);
			
			//rand_string.setLength(0);
	    }
	}
	
	void FillOutExtraSet() {
		for (int i = 0; i < extraSetSize; i++) {
	    	extra_randomStringSet[i] = randomString();
	    	//rand_string.setLength(0);
	    }
	}
	
	String randomString() {
		
		String alphabet = "qwertyuiopasdfghjklzxcvbnm1234567890QWERTYUIOPASDFGHJKLZXCVBNM";
		char cha;
		int random_word_lenght = 0;
		
		while (random_word_lenght < 15) {
			random_word_lenght = generator.nextInt(alphabet.length());
		}
		
		String new_generated_word = "";
		boolean isSimilar = true;
		
		while (isSimilar) {
			isSimilar = false;
		    for(int i = 0; i < random_word_lenght; i++) {
		        cha = alphabet.charAt(generator.nextInt(alphabet.length()));
		        rand_string.append(cha);
		    }
			
			new_generated_word = rand_string.toString();
			
			for (int i = 0; i < randomStringSet.length; i++) {
				if (randomStringSet[i] != null && randomStringSet[i].equals(new_generated_word)) {
					isSimilar = true;
					break;
				}
			}
		
			rand_string.setLength(0);
			
			if (!isSimilar) { break; }
		}
	    
	    return new_generated_word;
	}
	
	void FillOutQuerySet() {
		
		int index;
		
		for (int i = 0; i < setSize; i++) {
			index = generator.nextInt(setSize);
			queryWordSet[i] = randomStringSet[index];
		}
		
		for (int i = setSize; i < querySize; i++) {
			index = generator.nextInt(extraSetSize);
			queryWordSet[i] = extra_randomStringSet[index];
		}
	}
	
	void RunEvaluation() {
		
		String query_word;
		int det_counter = 0, ran_counter = 0;		
		
		for (int i = 0; i < querySize; i++) {
			
			query_word = queryWordSet[i];
			
			if (bloomFilterDet.Appears(query_word) == true) {
				if (CheckInExtraRandomStringSet(query_word) == true) {
					det_counter++;
				}
			}
			
			if (bloomFilterRan.Appears(query_word) == true) {
				if (CheckInExtraRandomStringSet(query_word) == true) {
					ran_counter++;
				}
			}
		}
		
		System.out.println("\nEvaluation results");
		System.out.println("Number of bits per element: " + bitsPerElement);
		
		System.out.println(String.format("Deterministic FalsePositive result: %.5f", (double) det_counter / querySize));
		System.out.println(String.format("Random FalsePositive result: %.5f", (double) ran_counter / querySize));
		System.out.println(String.format("Theory result: %.5f", Math.pow(0.618, bitsPerElement)));
	}
	
	boolean CheckInExtraRandomStringSet(String query_word) {
		
		for (int i = 0; i < extraSetSize; i++) {
			if (extra_randomStringSet[i].equals(query_word)) {
				return true;
			}
		}
		
		return false;
	}
}
